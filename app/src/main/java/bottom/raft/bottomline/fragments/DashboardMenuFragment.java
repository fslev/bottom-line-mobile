package bottom.raft.bottomline.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.activities.AddressActivity;
import bottom.raft.bottomline.activities.App;
import bottom.raft.bottomline.activities.EnergyConsumptionActivity;
import bottom.raft.bottomline.activities.HeatConsumptionActivity;
import bottom.raft.bottomline.activities.MainActivity;
import bottom.raft.bottomline.activities.UtilityExpenseActivity;
import bottom.raft.bottomline.activities.WaterConsumptionActivity;


public class DashboardMenuFragment extends Fragment {

    public static final Logger log = Logger.getLogger(DashboardMenuFragment.class.getName());

    View waterMenuItem;
    View energyMenuItem;
    View heatMenuItem;
    View expenseMenuItem;
    View addressesMenuItem;
    View homeMenuItem;

    TextView countryTextView;
    TextView cityTextView;
    TextView streetTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard_menu, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        waterMenuItem = getActivity().findViewById(R.id.waterMenuItemId);
        energyMenuItem = getActivity().findViewById(R.id.energyMenuItemId);
        heatMenuItem = getActivity().findViewById(R.id.heatMenuItemId);
        expenseMenuItem = getActivity().findViewById(R.id.expenseMenuItemId);
        addressesMenuItem = getActivity().findViewById(R.id.addressesMenuItemId);
        homeMenuItem = getActivity().findViewById(R.id.homeMenuItemId);

        countryTextView = (TextView) getActivity().findViewById(R.id.countryMenuId);
        cityTextView = (TextView) getActivity().findViewById(R.id.cityMenuId);
        streetTextView = (TextView) getActivity().findViewById(R.id.streetMenuId);

        waterMenuItem.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {
                                                 log.info("Starting water consumption activity");
                                                 Intent intent = new Intent(getActivity(), WaterConsumptionActivity.class);
                                                 getActivity().finish();
                                                 getActivity().startActivity(intent);
                                             }
                                         }
        );
        energyMenuItem.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  log.info("Starting energy consumption activity");
                                                  Intent intent = new Intent(getActivity(), EnergyConsumptionActivity.class);
                                                  getActivity().finish();
                                                  getActivity().startActivity(intent);
                                              }
                                          }
        );
        heatMenuItem.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                log.info("Starting heat consumption activity");
                                                Intent intent = new Intent(getActivity(), HeatConsumptionActivity.class);
                                                getActivity().finish();
                                                getActivity().startActivity(intent);
                                            }
                                        }
        );
        expenseMenuItem.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
                                                   log.info("Starting utilities expense activity");
                                                   Intent intent = new Intent(getActivity(), UtilityExpenseActivity.class);
                                                   getActivity().finish();
                                                   getActivity().startActivity(intent);
                                               }
                                           }
        );
        addressesMenuItem.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     log.info("Starting addresses activity");
                                                     Intent intent = new Intent(getActivity(), AddressActivity.class);
                                                     getActivity().finish();
                                                     getActivity().startActivity(intent);
                                                 }
                                             }
        );
        homeMenuItem.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                log.info("Starting main activity");
                                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                                getActivity().finish();
                                                getActivity().startActivity(intent);
                                            }
                                        }
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        setupSelectedAddressView();
    }

    private void setupSelectedAddressView() {
        try {
            JSONObject addressJson = new JSONObject(((App) getActivity().getApplication()).getSelectedAddress());
            countryTextView.setText(addressJson.getString("country"));
            cityTextView.setText(addressJson.getString("city"));
            streetTextView.setText(addressJson.getString("street"));
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }

    }
}

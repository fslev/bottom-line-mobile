package bottom.raft.bottomline.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;

/**
 * Created by raft on 19.01.2016.
 */
public class EnergyListAdapter extends BaseAdapter {
    public static final Logger log = Logger.getLogger(EnergyListAdapter.class.getName());
    private Context context;
    private JSONArray list;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM,yyyy");

    public EnergyListAdapter(Context context, JSONArray list) {
        super();
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return list.getJSONObject(position);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_energy, parent, false);
        TextView date = (TextView) rowView.findViewById(R.id.dateId);
        TextView index = (TextView) rowView.findViewById(R.id.indexId);
        TextView cons = (TextView) rowView.findViewById(R.id.consId);
        TextView desc = (TextView) rowView.findViewById(R.id.descId);

        try {
            JSONObject jsonEnergy = list.getJSONObject(position);
            long dateInMillis = jsonEnergy.getLong("date");
            ViewUtil.setTextViewValue(date, dateFormat.format(new Date(dateInMillis)));
            ViewUtil.setTextViewValue(index, jsonEnergy.getString("index"));
            ViewUtil.setTextViewValue(cons, jsonEnergy.getString("consumption"));
            ViewUtil.setTextViewValue(desc, jsonEnergy.getString("description"));
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
        return rowView;
    }
}

package bottom.raft.bottomline.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;

/**
 * Created by raft on 19.01.2016.
 */
public class WaterListAdapter extends BaseAdapter {
    public static final Logger log = Logger.getLogger(WaterListAdapter.class.getName());
    private Context context;
    private JSONArray list;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM,yyyy");

    public WaterListAdapter(Context context, JSONArray list) {
        super();
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return list.getJSONObject(position);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_water, parent, false);
        TextView date = (TextView) rowView.findViewById(R.id.dateId);
        TextView index = (TextView) rowView.findViewById(R.id.indexId);
        TextView cons = (TextView) rowView.findViewById(R.id.consId);
        TextView type = (TextView) rowView.findViewById(R.id.typeId);
        TextView room = (TextView) rowView.findViewById(R.id.roomId);
        TextView desc = (TextView) rowView.findViewById(R.id.descId);

        try {
            JSONObject jsonWater = list.getJSONObject(position);
            long dateInMillis = jsonWater.getLong("date");
            ViewUtil.setTextViewValue(date, dateFormat.format(new Date(dateInMillis)));
            ViewUtil.setTextViewValue(index, jsonWater.getString("index"));
            ViewUtil.setTextViewValue(cons, jsonWater.getString("consumption"));
            ViewUtil.setTextViewValue(desc, jsonWater.getString("description"));
            String typeVal = jsonWater.getString("type");
            if (typeVal.equals("HOT")) {
                type.setTextColor(0xFFf46b3d);
            } else {
                type.setTextColor(0xFF69cdd4);
            }
            ViewUtil.setTextViewValue(type, typeVal);
            ViewUtil.setTextViewValue(room, jsonWater.getJSONObject("room").getString("name"));
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
        return rowView;
    }
}

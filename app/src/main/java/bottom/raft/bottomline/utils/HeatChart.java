package bottom.raft.bottomline.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raft on 19.02.2016.
 */
public class HeatChart extends AbstractChart {
    public HeatChart(JSONArray jsonData) throws JSONException {
        super(jsonData);
    }

    @Override
    protected List<String> extractLabelsFromJson(JSONObject heatConsJson) throws JSONException {
        List<String> labels = new ArrayList<>();
        labels.add("Total");
        labels.add(heatConsJson.getJSONObject("room").getString("name"));
        return labels;
    }
}

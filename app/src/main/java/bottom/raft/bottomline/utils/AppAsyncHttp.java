package bottom.raft.bottomline.utils;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.activities.App;
import bottom.raft.bottomline.activities.MainActivity;

/**
 * Created by raft on 17.01.2016.
 */
public class AppAsyncHttp extends AsyncTask<HttpRequestBase, Void, CustomHttpResponse> {

    public static final String URL_API = "http://house-angrynerds.rhcloud.com/api";
    public static final String GENERAL_ERROR_MSG = "Something is wrong";
    public static final String GENERAL_BUSINESS_MSG = "Cannot get data";

    public static Logger log = Logger.getLogger(AppAsyncHttp.class.getName());


    private HttpClient client = new DefaultHttpClient();
    private Context context;

    public AppAsyncHttp(Context context) {
        this.context = context;
    }

    public void login(String data) throws WebAppException, JSONException, UnsupportedEncodingException {
        log.info("Login: " + data);
        HttpPost postRequest = new HttpPost(URL_API + "/auth/login");
        postRequest.setEntity(new StringEntity(data));
        postRequest.addHeader("Content-Type", "application/json");
        execute(postRequest);
    }

    public void createUser(String data) throws WebAppException, JSONException, UnsupportedEncodingException {
        log.info("Login: " + data);
        HttpPost postRequest = new HttpPost(URL_API + "/person");
        postRequest.setEntity(new StringEntity(trimData(data)));
        postRequest.addHeader("Content-Type", "application/json");
        execute(postRequest);
    }

    public void getAddresses() {
        HttpGet request = new HttpGet(URL_API + "/address/user/" + ((App) this.context.getApplicationContext()).getSessionUserId());
        request.setHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(request);
    }

    public void getAddress(int addressId) {
        HttpGet request = new HttpGet(URL_API + "/address/" + addressId);
        request.setHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(request);
    }

    public void addAddress(String data) throws UnsupportedEncodingException {
        log.info("Add address " + data);
        HttpPost postRequest = new HttpPost(URL_API + "/address");
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        postRequest.setEntity(new StringEntity(trimData(data)));
        execute(postRequest);
    }

    public void updateAddress(String data, int addressId) throws UnsupportedEncodingException {
        log.info("Update address " + data + " with id " + addressId);
        HttpPut putRequest = new HttpPut(URL_API + "/address/" + addressId);
        putRequest.addHeader("Content-Type", "application/json");
        putRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        putRequest.setEntity(new StringEntity(trimData(data)));
        execute(putRequest);
    }

    public void deleteAddress(int id) {
        log.info("Delete address entry with id " + id);
        HttpDelete delRequest = new HttpDelete(URL_API + "/address/" + id);
        delRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(delRequest);
    }

    public void addRoom(String data, int addressId) throws UnsupportedEncodingException {
        log.info("Add room " + data);
        HttpPost postRequest = new HttpPost(URL_API + "/room/address/" + addressId);
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        postRequest.setEntity(new StringEntity(trimData(data)));
        execute(postRequest);
    }

    public void updateRoom(int roomId, String data) throws UnsupportedEncodingException {
        log.info("Update room " + data);
        HttpPut putRequest = new HttpPut(URL_API + "/room/" + roomId);
        putRequest.addHeader("Content-Type", "application/json");
        putRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        putRequest.setEntity(new StringEntity(trimData(data)));
        execute(putRequest);
    }

    public void deleteRoom(int id) {
        log.info("Delete room entry with id " + id);
        HttpDelete delRequest = new HttpDelete(URL_API + "/room/" + id);
        delRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(delRequest);
    }

    public void addUtility(String data, int addressId) throws UnsupportedEncodingException {
        log.info("Add utility " + data);
        HttpPost postRequest = new HttpPost(URL_API + "/utility/address/" + addressId);
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        postRequest.setEntity(new StringEntity(trimData(data)));
        execute(postRequest);
    }

    public void updateUtility(int utilityId, String data) throws UnsupportedEncodingException {
        log.info("Update utility " + data);
        HttpPut putRequest = new HttpPut(URL_API + "/utility/" + utilityId);
        putRequest.addHeader("Content-Type", "application/json");
        putRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        putRequest.setEntity(new StringEntity(trimData(data)));
        execute(putRequest);
    }

    public void deleteUtility(int id) {
        log.info("Delete utility entry with id " + id);
        HttpDelete delRequest = new HttpDelete(URL_API + "/utility/" + id);
        delRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(delRequest);
    }

    public void getWaterConsList(int addressId, int offset, int limit) throws UnsupportedEncodingException {
        getWaterConsList(addressId, offset, limit, null);
    }

    public void getWaterConsList(int addressId, int offset, int limit, String pattern) throws UnsupportedEncodingException {
        HttpGet request = new HttpGet(URL_API + "/wconsumption?addressId=" + addressId + "&offset="
                + offset + "&limit=" + limit + "&pattern=" + ((pattern != null) ?
                URLEncoder.encode(pattern, "UTF-8") : pattern));
        request.setHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(request);
    }

    public void addWaterCons(String data, int addressId, int roomId) throws UnsupportedEncodingException {
        log.info("Add water consumption " + data + " for addressId " + addressId + " and roomId " + roomId);
        HttpPost postRequest = new HttpPost(URL_API + "/wconsumption/address/" + addressId + "/room/" + roomId);
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        postRequest.setEntity(new StringEntity(trimData(data)));
        execute(postRequest);
    }

    public void updateWaterCons(String data, int id, int roomId) throws UnsupportedEncodingException {
        log.info("Update water consumption " + data + " with id " + id + " and roomId " + roomId);
        HttpPut putRequest = new HttpPut(URL_API + "/wconsumption/" + id + "/room/" + roomId);
        putRequest.addHeader("Content-Type", "application/json");
        putRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        putRequest.setEntity(new StringEntity(trimData(data)));
        execute(putRequest);
    }

    public void deleteWaterCons(int id) {
        log.info("Delete water consumption entry with id " + id);
        HttpDelete delRequest = new HttpDelete(URL_API + "/wconsumption/" + id);
        delRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(delRequest);
    }

    public void getHeatConsList(int addressId, int offset, int limit) throws UnsupportedEncodingException {
        getHeatConsList(addressId, offset, limit, null);
    }

    public void getHeatConsList(int addressId, int offset, int limit, String pattern) throws UnsupportedEncodingException {
        HttpGet request = new HttpGet(URL_API + "/hconsumption?addressId=" + addressId + "&offset="
                + offset + "&limit=" + limit + "&pattern=" + ((pattern != null) ?
                URLEncoder.encode(pattern, "UTF-8") : pattern));
        request.setHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(request);
    }

    public void addHeatCons(String data, int addressId, int roomId) throws UnsupportedEncodingException {
        log.info("Add heat consumption " + data + " for addressId " + addressId + " and roomId " + roomId);
        HttpPost postRequest = new HttpPost(URL_API + "/hconsumption/address/" + addressId + "/room/" + roomId);
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        postRequest.setEntity(new StringEntity(trimData(data)));
        execute(postRequest);
    }

    public void updateHeatCons(String data, int id, int roomId) throws UnsupportedEncodingException {
        log.info("Update heat consumption " + data + " with id " + id + " and roomId " + roomId);
        HttpPut putRequest = new HttpPut(URL_API + "/hconsumption/" + id + "/room/" + roomId);
        putRequest.addHeader("Content-Type", "application/json");
        putRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        putRequest.setEntity(new StringEntity(trimData(data)));
        execute(putRequest);
    }

    public void deleteHeatCons(int id) {
        log.info("Delete heat consumption entry with id " + id);
        HttpDelete delRequest = new HttpDelete(URL_API + "/hconsumption/" + id);
        delRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(delRequest);
    }


    public void getEnergyConsList(int addressId, int offset, int limit) throws UnsupportedEncodingException {
        getEnergyConsList(addressId, offset, limit, null);
    }

    public void getEnergyConsList(int addressId, int offset, int limit, String pattern) throws UnsupportedEncodingException {
        HttpGet request = new HttpGet(URL_API + "/econsumption?addressId=" + addressId + "&offset="
                + offset + "&limit=" + limit + "&pattern=" + ((pattern != null) ?
                URLEncoder.encode(pattern, "UTF-8") : pattern));
        request.setHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(request);
    }

    public void addEnergyCons(String data, int addressId) throws UnsupportedEncodingException {
        log.info("Add energy consumption " + data + " for addressId " + addressId);
        HttpPost postRequest = new HttpPost(URL_API + "/econsumption/address/" + addressId);
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        postRequest.setEntity(new StringEntity(trimData(data)));
        execute(postRequest);
    }

    public void updateEnergyCons(String data, int id) throws UnsupportedEncodingException {
        log.info("Update energy consumption " + data + " with id " + id);
        HttpPut putRequest = new HttpPut(URL_API + "/econsumption/" + id);
        putRequest.addHeader("Content-Type", "application/json");
        putRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        putRequest.setEntity(new StringEntity(trimData(data)));
        execute(putRequest);
    }

    public void deleteEnergyCons(int id) {
        log.info("Delete energy consumption entry with id " + id);
        HttpDelete delRequest = new HttpDelete(URL_API + "/econsumption/" + id);
        delRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(delRequest);
    }


    public void getUtilityExpList(int addressId, int offset, int limit, long startDate, long endDate) throws UnsupportedEncodingException {
        getUtilityExpList(addressId, offset, limit, startDate, endDate, null);
    }

    public void getUtilityExpList(int addressId, int offset, int limit, long startDate, long endDate, String pattern) throws UnsupportedEncodingException {
        HttpGet request = new HttpGet(URL_API + "/utilityexp?addressId=" + addressId + "&offset="
                + offset + "&limit=" + limit + "&startDate=" + startDate + "&endDate=" + endDate +
                "&pattern=" + ((pattern != null) ?
                URLEncoder.encode(pattern, "UTF-8") : pattern));
        request.setHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(request);
    }

    public void addUtilityExpense(String data, int utilityId) throws UnsupportedEncodingException {
        log.info("Add utility expense " + data + " for utilityId " + utilityId);
        HttpPost postRequest = new HttpPost(URL_API + "/utilityexp/utility/" + utilityId);
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        postRequest.setEntity(new StringEntity(trimData(data)));
        execute(postRequest);
    }

    public void updateUtilityExpense(String data, int utilityExpId) throws UnsupportedEncodingException {
        log.info("Update utility expense: " + trimData(data) + " with id " + utilityExpId);
        HttpPut putRequest = new HttpPut(URL_API + "/utilityexp/" + utilityExpId);
        putRequest.addHeader("Content-Type", "application/json");
        putRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        putRequest.setEntity(new StringEntity(trimData(data)));
        execute(putRequest);
    }

    public void deleteUtilityExpense(int id) {
        log.info("Delete utility expense entry with id " + id);
        HttpDelete delRequest = new HttpDelete(URL_API + "/utilityexp/" + id);
        delRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        execute(delRequest);
    }

    public void sendMail(String data) throws UnsupportedEncodingException {
        log.info("Sending email... " + data);
        HttpPost postRequest = new HttpPost(URL_API + "/mail");
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("authorization", ((App) this.context.getApplicationContext()).getAuthorization());
        postRequest.setEntity(new StringEntity(data));
        execute(postRequest);
    }

    @Override
    protected CustomHttpResponse doInBackground(HttpRequestBase[] params) {
        try {
            HttpResponse response = client.execute(params[0]);
            log.info("Making http call with authorization: " + ((App) this.context.getApplicationContext()).getAuthorization());
            int code = response.getStatusLine().getStatusCode();
            if (code == HttpStatus.SC_UNAUTHORIZED && !(context instanceof MainActivity)) {
                log.log(Level.WARNING, "Caught unauthorized response");
                ((App) this.context.getApplicationContext()).clearSession();
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("unauthorized_message", "Token expired");
                context.startActivity(intent);
                ((Activity) context).finish();
                return null;
            }
            return new CustomHttpResponse(code, EntityUtils.toString(response.getEntity()), response.getAllHeaders());
        } catch (UnknownHostException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            return new CustomHttpResponse(HttpStatus.SC_SERVICE_UNAVAILABLE, "Check internet connection", null);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    private String trimData(String data) {
        JSONObject dataJsonObj = null;
        try {
            dataJsonObj = new JSONObject(data);
            Iterator<String> it = dataJsonObj.keys();
            while (it.hasNext()) {
                String key = it.next();
                if (!(dataJsonObj.get(key) instanceof JSONObject || !(dataJsonObj.get(key) instanceof JSONArray))) {
                    dataJsonObj.put(key, dataJsonObj.get(key).toString().trim());
                }
            }
        } catch (JSONException e) {
            log.log(Level.WARNING, "Cannot trim data. Not a json object");
            return data;
        }
        return dataJsonObj.toString();
    }
}

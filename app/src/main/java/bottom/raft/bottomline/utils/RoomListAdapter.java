package bottom.raft.bottomline.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bottom.raft.bottomline.R;

/**
 * Created by raft on 19.01.2016.
 */
public class RoomListAdapter extends BaseAdapter {

    private Context context;
    private JSONArray list;

    public RoomListAdapter(Context context, JSONArray list) {
        super();
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.length();
    }

    @Override
    public Object getItem(int position) {
        try {
            return list.getJSONObject(position);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.simple_item, parent, false);
        TextView itemText = (TextView) rowView.findViewById(R.id.itemId);

        try {
            JSONObject jsonRoom = list.getJSONObject(position);
            itemText.setText(jsonRoom.getString("name"));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return rowView;
    }
}

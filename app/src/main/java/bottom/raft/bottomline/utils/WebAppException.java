package bottom.raft.bottomline.utils;

/**
 * Created by raft on 17.01.2016.
 */
public class WebAppException extends Exception {

    public WebAppException(String msg) {
        super(msg);
    }
}
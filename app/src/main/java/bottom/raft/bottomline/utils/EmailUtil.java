package bottom.raft.bottomline.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by raft on 28.02.2016.
 */
public class EmailUtil {

    public static final String FROM = "angry.nerds.house@gmail.com";

    public static JSONObject buildEmailJson(JSONArray recipients, String subject, String body) throws JSONException {
        JSONObject emailJson = new JSONObject();
        emailJson.put("from", FROM);
        emailJson.put("subject", subject);
        emailJson.put("msg", body);
        emailJson.put("recipients", recipients);
        return emailJson;
    }
}

package bottom.raft.bottomline.utils;

/**
 * Created by raft on 17.01.2016.
 */
public class Address {

    private String country;

    private String city;
    private String street;
    private int id;

    public Address(String country, String city, String street, int id) {
        this.city = city;
        this.country = country;
        this.street = street;
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return country + ", " + city + "\n" + street;
    }
}

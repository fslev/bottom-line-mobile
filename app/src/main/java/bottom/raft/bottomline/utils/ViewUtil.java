package bottom.raft.bottomline.utils;

import android.widget.TextView;

import org.json.JSONException;

/**
 * Created by raft on 27.02.2016.
 */
public class ViewUtil {

    public static void setTextViewValue(TextView textView, Object val) throws JSONException {
        textView.setText((val != null && !val.toString().equals("null") && !val.toString().isEmpty()) ? val.toString() : null);
    }
}

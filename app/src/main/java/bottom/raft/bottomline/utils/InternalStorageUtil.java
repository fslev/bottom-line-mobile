package bottom.raft.bottomline.utils;

import android.content.Context;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by raft on 28.02.2016.
 */
public class InternalStorageUtil {
    public static final Logger log = Logger.getLogger(InternalStorageUtil.class.getName());
    public static final String FILENAME = "login";

    public static void persistProperties(Context context, Properties props) {
        String data = "";
        for (String key : props.stringPropertyNames()) {
            data += key + "=" + props.getProperty(key) + "\n";
        }

        FileOutputStream outputStream = null;
        try {
            outputStream = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public static Properties readProperties(Context context) {
        Properties props = new Properties();
        try {
            props.load(context.openFileInput(FILENAME));
        } catch (IOException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
        return props;
    }
}

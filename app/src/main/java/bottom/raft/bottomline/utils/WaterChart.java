package bottom.raft.bottomline.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raft on 19.02.2016.
 */
public class WaterChart extends AbstractChart {
    public WaterChart(JSONArray jsonData) throws JSONException {
        super(jsonData);
    }

    @Override
    protected List<String> extractLabelsFromJson(JSONObject waterConsJson) throws JSONException {
        List<String> labels = new ArrayList<>();
        labels.add("Total");
        labels.add(waterConsJson.getString("type"));
        labels.add(waterConsJson.getJSONObject("room").getString("name"));
        labels.add(waterConsJson.getString("type") + " & " + waterConsJson.getJSONObject("room").getString("name"));
        return labels;
    }
}

package bottom.raft.bottomline.utils;


import com.github.mikephil.charting.data.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by raft on 18.02.2016.
 */
public abstract class AbstractChart {

    public static final List<Integer> colors = new ArrayList<Integer>(Arrays.asList(
            new Integer[]{0xFF000000, 0xFFFF0000, 0xFF00FF00, 0xFF00FFFF,
                    0xFFFF00FF, 0xFF0000FF, 0xFF9683EC, 0xFFFF6A6A, 0xFF888888, 0xFFFFEB7F, 0xFFA9BA9D,
                    0xFF490560, 0xFF002B1B, 0xFF8B3A3A}));

    private Map<String, Map<Long, Double>> chartData = new HashMap<>();

    private List<Long> timeline;
    private List<String> xLabels = new ArrayList<>();
    private Map<String, List<Entry>> dataSets = new HashMap<>();

    public AbstractChart(JSONArray jsonData) throws JSONException {
        this.chartData = generateChartData(jsonData);
        this.timeline = generateTimeline(this.chartData);
        this.xLabels = generateXLabels(this.timeline);
        this.dataSets = generateDataSets(this.chartData, this.timeline);
    }

    protected Map<String, Map<Long, Double>> generateChartData(JSONArray jsonData) throws JSONException {
        Map<String, Map<Long, Double>> chartData = new HashMap<>();

        for (int i = 0; i < jsonData.length(); i++) {
            JSONObject jsonElement = jsonData.getJSONObject(i);

            long date = jsonElement.getLong("date");
            double value = jsonElement.optDouble("consumption", 0);

            List<String> labels = extractLabelsFromJson(jsonElement);

            for (String label : labels) {
                if (chartData.get(label) == null) {
                    chartData.put(label, new HashMap<Long, Double>());
                }
                chartData.get(label).put(date, chartData.get(label).get(date) == null
                        ? value : chartData.get(label).get(date) + value);
            }
        }
        return chartData;
    }

    private List<Long> generateTimeline(Map<String, Map<Long, Double>> chartData) {
        Set<Long> set = new HashSet<>();
        for (Map.Entry<String, Map<Long, Double>> entry : chartData.entrySet()) {
            for (Long date : entry.getValue().keySet()) {
                set.add(date);
            }
        }
        List<Long> timeline = new ArrayList<>(set);
        Collections.sort(timeline);
        return timeline;
    }

    private List<String> generateXLabels(List<Long> timeline) {
        List<String> list = new ArrayList<>();
        SimpleDateFormat sd = new SimpleDateFormat("d.MMM''yy");
        for (Long date : timeline) {
            list.add(sd.format(date));
        }
        return list;
    }

    private Map<String, List<Entry>> generateDataSets(Map<String, Map<Long, Double>> chartData, List<Long> timeline) {
        Map<String, List<Entry>> dataSets = new HashMap<>();
        for (Map.Entry<String, Map<Long, Double>> entry : chartData.entrySet()) {
            String dataSetLabel = entry.getKey();
            List<Entry> dataSetValues = new ArrayList<>();
            for (Map.Entry<Long, Double> datapointEntry : entry.getValue().entrySet()) {
                dataSetValues.add(new Entry(Float.valueOf(datapointEntry.getValue().toString()), timeline.indexOf(datapointEntry.getKey())));
            }
            Collections.sort(dataSetValues, new EntryComparator());
            dataSets.put(dataSetLabel, dataSetValues);
        }
        return dataSets;
    }

    public List<String> getxLabels() {
        return this.xLabels;
    }

    public Map<String, List<Entry>> getDataSets() {
        return dataSets;
    }

    /**
     * Generate labels from the keys of a json object: "total", key value and combination
     * of key values
     *
     * @param jsonObject
     * @return List of labes
     */
    protected abstract List<String> extractLabelsFromJson(JSONObject jsonObject) throws JSONException;


    class EntryComparator implements Comparator<Entry> {

        @Override
        public int compare(Entry lhs, Entry rhs) {
            if (lhs.getXIndex() > rhs.getXIndex()) {
                return 1;
            }
            if (lhs.getXIndex() < rhs.getXIndex()) {
                return -1;
            }
            return 0;
        }
    }
}

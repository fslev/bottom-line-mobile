package bottom.raft.bottomline.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by raft on 19.02.2016.
 */
public class UtilityExpChart extends AbstractChart {
    public UtilityExpChart(JSONArray jsonData) throws JSONException {
        super(jsonData);
    }

    @Override
    protected List<String> extractLabelsFromJson(JSONObject heatConsJson) throws JSONException {
        List<String> labels = new ArrayList<>();
        labels.add("Total");
        labels.add(heatConsJson.getJSONObject("utility").getString("name"));
        return labels;
    }

    @Override
    protected Map<String, Map<Long, Double>> generateChartData(JSONArray jsonData) throws JSONException {
        Map<String, Map<Long, Double>> chartData = new HashMap<>();

        for (int i = 0; i < jsonData.length(); i++) {
            JSONObject jsonElement = jsonData.getJSONObject(i);

            long date = jsonElement.getLong("date");
            double value = jsonElement.optDouble("value", 0);

            List<String> labels = extractLabelsFromJson(jsonElement);

            for (String label : labels) {
                if (chartData.get(label) == null) {
                    chartData.put(label, new HashMap<Long, Double>());
                }
                chartData.get(label).put(date, chartData.get(label).get(date) == null
                        ? value : chartData.get(label).get(date) + value);
            }
        }
        return chartData;
    }
}

package bottom.raft.bottomline.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;

public class AddressEditActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(AddressEditActivity.class.getName());

    private TextView countryText;
    private TextView cityText;
    private TextView streetText;
    private View addressConfigLayout;
    private TextView addrConfigTitleText;

    private String actionType;
    private JSONObject selectedAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_edit);

        countryText = (TextView) findViewById(R.id.countryId);
        cityText = (TextView) findViewById(R.id.cityId);
        streetText = (TextView) findViewById(R.id.streetId);
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        addressConfigLayout = findViewById(R.id.addressConfigLayoutId);
        addrConfigTitleText = (TextView) findViewById(R.id.configTitleId);

        hideMessage();

        actionType = getIntent().getStringExtra("actionType");
    }


    @Override
    protected void onStart() {
        super.onStart();
        switch (actionType) {
            case "add":
                addrConfigTitleText.setText("Add address");
                addressConfigLayout.setVisibility(View.GONE);
                break;
            case "edit":
                addrConfigTitleText.setText("Edit address");
                setSelectedAddress();
                populateFields(selectedAddress);
                addressConfigLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setSelectedAddress() {
        try {
            selectedAddress = new JSONObject(((App) getApplication()).getSelectedAddress());
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    private void populateFields(JSONObject addressJson) {
        try {
            countryText.setText(addressJson.getString("country"));
            cityText.setText(addressJson.getString("city"));
            streetText.setText(addressJson.getString("street"));
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    public void apply(View view) {
        if (countryText.getText().length() == 0 || cityText.getText().length() == 0 || streetText.getText().length() == 0) {
            showMessage("No empty fields are allowed");
            return;
        }
        JSONObject address = null;
        try {
            address = buildJsonData();
            switch (actionType) {
                case "add":
                    addAddress(address.toString());
                    break;
                case "edit":
                    updateAddress(address.toString(), selectedAddress.getInt("id"));
                    break;
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }

    }

    private void addAddress(final String address) throws UnsupportedEncodingException {
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS. Go back and edit/configure this address");
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.addAddress(address);
    }

    private void updateAddress(String address, int addressId) throws UnsupportedEncodingException {
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                    refreshSelectedAddress();
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.updateAddress(address, addressId);
    }


    private JSONObject buildJsonData() throws JSONException, ParseException {
        JSONObject json = new JSONObject();
        json.put("country", countryText.getText());
        json.put("city", cityText.getText());
        json.put("street", streetText.getText());
        return json;
    }

    public void configureRooms(View view) {
        Intent intent = new Intent(this, RoomActivity.class);
        startActivity(intent);
    }

    public void configureUtilities(View view) {
        Intent intent = new Intent(this, UtilityActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, AddressActivity.class));
        finish();
    }
}

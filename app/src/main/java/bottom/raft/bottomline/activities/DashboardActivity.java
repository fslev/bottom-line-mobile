package bottom.raft.bottomline.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.logging.Logger;

import bottom.raft.bottomline.R;

public class DashboardActivity extends AppCompatActivity {

    public static final Logger log = Logger.getLogger(DashboardActivity.class.getName());

    public static final int MAX_RESULTS = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this, AddressActivity.class));
    }

    /**
     * No use of it
     * Could prove useful
     */

    /*
    private void startWaterEditFragment() {
        WaterConsEditFragment fragment = new WaterConsEditFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentEditId, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }*/
}

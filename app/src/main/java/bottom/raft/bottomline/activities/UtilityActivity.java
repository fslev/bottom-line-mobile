package bottom.raft.bottomline.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.UtilityListAdapter;

/**
 * Created by raft on 12.02.2016.
 */
public class UtilityActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(UtilityActivity.class.getName());

    private TextView countryText;
    private TextView cityText;
    private TextView streetText;

    ListView utilityListView;

    private JSONObject selectedUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility);

        countryText = (TextView) findViewById(R.id.countryId);
        cityText = (TextView) findViewById(R.id.cityId);
        streetText = (TextView) findViewById(R.id.streetId);
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        utilityListView = (ListView) findViewById(R.id.utilityListViewId);

        editBtn = findViewById(R.id.feditBtnId);
        rmBtn = findViewById(R.id.frmBtnId);
    }

    @Override
    protected void onStart() {
        super.onStart();
        selectedUtility = null;
        hideButtons();
        hideMessage();
        try {
            ((App) getApplication()).populateSelectedAddress(countryText, cityText, streetText);
            refreshSelectedAddressAndPopulateUtilityList();
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    private void refreshSelectedAddressAndPopulateUtilityList() throws JSONException {
        selectedUtility = null;
        new AppAsyncHttp(this) {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Refresh address and get utilities...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                hideMessage();
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    ((App) getApplication()).setSelectedAddress(customHttpResponse.entity);
                    log.info("Selected address refreshed: " + ((App) getApplication()).getSelectedAddress());

                    JSONArray utilitiesJsonArray = null;
                    try {
                        utilitiesJsonArray = ((App) getApplication()).getSelectedAddressUtilityList();
                        log.info("Got utilities from selected address: " + utilitiesJsonArray.toString());
                    } catch (JSONException e) {
                        log.log(Level.SEVERE, e.getMessage(), e);
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }
                    if (utilitiesJsonArray == null) {
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }
                    UtilityListAdapter adapter = new UtilityListAdapter(getApplicationContext(), utilitiesJsonArray);
                    utilityListView.setAdapter(adapter);
                    utilityListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                            selectedUtility = ((JSONObject) utilityListView.getItemAtPosition(position));
                            log.info("Selected utility: " + selectedUtility.toString());
                            showButtons();
                        }
                    });
                    if (utilitiesJsonArray.length() == 0) {
                        showMessage("No utilities configured for this address");
                    } else {
                        showMessage("Choose a utility to configure");
                    }
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.getAddress(((App) getApplication()).getSelectedAddressId());
    }

    public void addItem(View view) {
        Intent intent = new Intent(this, UtilityEditActivity.class);
        intent.putExtra("actionType", "add");
        startActivity(intent);
    }

    public void editItem(View view) {
        Intent intent = new Intent(this, UtilityEditActivity.class);
        intent.putExtra("selectedUtility", selectedUtility.toString());
        intent.putExtra("actionType", "edit");
        startActivity(intent);
    }

    public void removeItem(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage("Delete entry?")
                .setIcon(R.drawable.emoticon_grumpy)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            removeUtility(selectedUtility.getInt("id"));
                            refreshSelectedAddressAndPopulateUtilityList();
                        } catch (JSONException e) {
                            log.log(Level.SEVERE, e.getMessage(), e);
                            showMessage(App.GENERAL_ERROR_MSG);
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    public void removeUtility(int id) {
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Removing...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == 200) {
                    showMessage("Utility successfully removed");
                } else if (customHttpResponse.code == 500) {
                    showMessage("Cannot remove entry");
                } else {
                    showMessage(customHttpResponse.entity);
                }
                selectedUtility = null;
                hideButtons();
            }
        }.deleteUtility(id);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

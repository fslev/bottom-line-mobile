package bottom.raft.bottomline.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;

public class UtilityEditActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(UtilityEditActivity.class.getName());

    private TextView utilityText;
    private View titleLayout;
    private TextView titleText;

    private String actionType;
    private JSONObject selectedUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility_edit);

        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        titleText = (TextView) findViewById(R.id.configTitleId);
        utilityText = (TextView) findViewById(R.id.utilityId);

        hideMessage();

        actionType = getIntent().getStringExtra("actionType");
    }


    @Override
    protected void onStart() {
        super.onStart();
        switch (actionType) {
            case "add":
                titleText.setText("Add utility");
                break;
            case "edit":
                titleText.setText("Edit utility");
                try {
                    selectedUtility = new JSONObject(getIntent().getStringExtra("selectedUtility"));
                } catch (JSONException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                populateFields(selectedUtility);
                break;
        }
    }

    private void populateFields(JSONObject utilityJson) {
        try {
            utilityText.setText(utilityJson.getString("name"));
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    public void apply(View view) {
        if (utilityText.getText().length() == 0) {
            showMessage("No empty fields are allowed");
            return;
        }
        JSONObject utilityJson = null;
        try {
            utilityJson = buildJsonData();
            switch (actionType) {
                case "add":
                    addUtility(utilityJson.toString(), ((App) getApplication()).getSelectedAddressId());
                    break;
                case "edit":
                    updateUtility(selectedUtility.getInt("id"), utilityJson.toString());
                    break;
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }

    }

    private void addUtility(String utility, int addressId) throws UnsupportedEncodingException {
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                    refreshSelectedAddress();
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.addUtility(utility, addressId);
    }

    private void updateUtility(int utilityId, String utility) throws UnsupportedEncodingException {
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                    refreshSelectedAddress();
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.updateUtility(utilityId, utility);
    }

    private JSONObject buildJsonData() throws JSONException, ParseException {
        JSONObject json = new JSONObject();
        json.put("name", utilityText.getText());
        return json;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

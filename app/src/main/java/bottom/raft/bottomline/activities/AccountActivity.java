package bottom.raft.bottomline.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.WebAppException;

/**
 * Created by raft on 16.02.2016.
 */
public class AccountActivity extends AppCompatActivity {

    public static final Logger log = Logger.getLogger(AccountActivity.class.getName());

    private EditText usernameEditText;
    private EditText nameEditText;
    private EditText emailEditText;
    private EditText pwdEditText;
    private EditText pwdAgainEditText;

    private View applyBtn;
    private View messageLayout;
    private TextView messageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);
        usernameEditText = (EditText) findViewById(R.id.usernameId);
        nameEditText = (EditText) findViewById(R.id.nameId);
        emailEditText = (EditText) findViewById(R.id.emailId);
        pwdEditText = (EditText) findViewById(R.id.pwdId);
        pwdAgainEditText = (EditText) findViewById(R.id.pwdAgainId);

        applyBtn = findViewById(R.id.fapplyBtnId);
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);

        listenToPwdChange();
        hideMessage();
    }

    public void apply(View view) {
        try {
            if (usernameEditText.getText().length() == 0 || nameEditText.getText().length() == 0 || pwdEditText.getText().length() == 0) {
                showMessage("No empty fields are allowed");
                return;
            }
            createUser();
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    private void createUser() throws JSONException, ParseException, UnsupportedEncodingException, WebAppException {
        final JSONObject data = buildJsonCreateUserData();
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("User created. Go back and login");
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.createUser(data.toString());
    }

    private void listenToPwdChange() {
        pwdEditText.addTextChangedListener(new PwdTextWatcher(pwdAgainEditText));
        pwdAgainEditText.addTextChangedListener(new PwdTextWatcher(pwdEditText));
    }


    private JSONObject buildJsonCreateUserData() throws JSONException, ParseException {
        JSONObject json = new JSONObject();
        json.put("username", usernameEditText.getText());
        json.put("name", nameEditText.getText());
        json.put("email", emailEditText.getText());
        json.put("password", pwdEditText.getText());
        return json;
    }

    protected void showMessage(String msg) {
        messageText.setText(msg);
        messageLayout.setVisibility(View.VISIBLE);
    }

    protected void hideMessage() {
        messageLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    class PwdTextWatcher implements TextWatcher {

        private EditText target;

        public PwdTextWatcher(EditText target) {
            this.target = target;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!target.getText().toString().equals((String.valueOf(s)))) {
                showMessage("Passwords must match !");
                applyBtn.setEnabled(false);
            } else {
                hideMessage();
                applyBtn.setEnabled(true);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }
}



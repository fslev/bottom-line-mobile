package bottom.raft.bottomline.activities;

import android.app.Application;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by raft on 03.02.2016.
 */
public class App extends Application {

    public static final String DATE_FORMAT = "d.MMM''yy";
    public static final String GENERAL_ERROR_MSG = "Ups, something went wrong!";
    public static final String GENERAL_LOADING_MSG = "Loading...";

    private String selectedAddress;
    private String sessionUser;
    private Integer sessionUserId;
    private String authorization;

    public String getSelectedAddress() {
        return selectedAddress;
    }

    public void setSelectedAddress(String selectedAddress) {
        this.selectedAddress = selectedAddress;
    }

    public String getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(String sessionUser) {
        this.sessionUser = sessionUser;
    }

    public int getSessionUserId() {
        return sessionUserId;
    }

    public void setSessionUserId(int userId) {
        this.sessionUserId = userId;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public void clearSession() {
        this.sessionUser = null;
        this.sessionUserId = null;
        this.authorization = null;
    }

    public void populateSelectedAddress(TextView countryText, TextView cityText, TextView streetText) throws JSONException {
        JSONObject addressJson = new JSONObject(selectedAddress);
        countryText.setText(addressJson.getString("country"));
        cityText.setText(addressJson.getString("city"));
        streetText.setText(addressJson.getString("street"));
    }

    public JSONArray getSelectedAddressRoomList() throws JSONException {
        JSONObject addressJson = new JSONObject(selectedAddress);
        return addressJson.getJSONArray("rooms");
    }

    public JSONArray getSelectedAddressUtilityList() throws JSONException {
        JSONObject addressJson = new JSONObject(selectedAddress);
        return addressJson.getJSONArray("utilities");
    }

    public int getSelectedAddressId() throws JSONException {
        JSONObject addressJson = new JSONObject(selectedAddress);
        return addressJson.getInt("id");
    }

    public String getSessionUsername() throws JSONException {
        JSONObject userJson = new JSONObject(sessionUser);
        return userJson.getString("username");
    }
}

package bottom.raft.bottomline.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.activities.charts.UtilityExpChartActivity;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.EmailUtil;
import bottom.raft.bottomline.utils.UtilitiesExpenseListAdapter;

public class UtilityExpenseActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(UtilityExpenseActivity.class.getName());

    public static final String UTILITY_EXP_MSG = "utilityExpMsg";

    private DrawerLayout navBarView;
    private ListView utilityExpList;
    private TextView pagesTextView;
    private View paginationLayoutView;

    private JSONObject selectedAddress;
    private JSONObject selectedUtilityExp;
    private int toggleNav;
    private int totalPages = 1;
    private int currPage = 1;
    private int offset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility_exp);
        setDashboardDrawerLayout();

        try {
            selectedAddress = new JSONObject(((App) getApplication()).getSelectedAddress());
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            return;
        }

        utilityExpList = (ListView) findViewById(R.id.utilityExpList);
        paginationLayoutView = findViewById(R.id.paginationLayoutId);
        pagesTextView = (TextView) findViewById(R.id.pagesTextId);
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        editBtn = findViewById(R.id.feditBtnId);
        rmBtn = findViewById(R.id.frmBtnId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideMessage();
        hideButtons();
        setPages(currPage, totalPages);
        getUtilitiesExpense(offset);
    }

    public void getUtilitiesExpense(final int offset) {
        try {
            new AppAsyncHttp(this) {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    showMessage(App.GENERAL_LOADING_MSG);
                }

                @Override
                protected void onPostExecute(CustomHttpResponse response) {
                    super.onPostExecute(response);
                    hideButtons();
                    if (response == null) {
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }

                    if (response.code == HttpStatus.SC_OK) {
                        JSONArray jsonArray = null;
                        try {
                            jsonArray = new JSONArray(response.entity);
                        } catch (JSONException e) {
                            showMessage(App.GENERAL_ERROR_MSG);
                            return;
                        }

                        int count = Integer.parseInt(response.getHeaderValue("count"));
                        totalPages = (count == 0) ? 1 : (int) Math.ceil((double) count / DashboardActivity.MAX_RESULTS);
                        setPages(currPage, totalPages);

                        UtilitiesExpenseListAdapter utilitiesExpAdapter = new UtilitiesExpenseListAdapter(getApplicationContext(), jsonArray);
                        utilityExpList.setAdapter(utilitiesExpAdapter);
                        utilityExpList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                                  @Override
                                                                  public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                                                                      selectedUtilityExp = ((JSONObject) utilityExpList.getItemAtPosition(position));
                                                                      showButtons();
                                                                  }
                                                              }
                        );
                        if (jsonArray.length() == 0 && offset == 0) {
                            showMessage("No entries. Please add one");
                        } else {
                            hideMessage();
                        }
                    } else {
                        showMessage(response.entity);
                    }

                }
            }.getUtilityExpList(selectedAddress.getInt("id"), offset, DashboardActivity.MAX_RESULTS,
                    1390867200000L, Calendar.getInstance().getTimeInMillis());
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }


    public void removeUtilityExp(final JSONObject utilityExp) {
        try {
            new AppAsyncHttp(this) {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    showMessage("Removing...");
                }

                @Override
                protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                    super.onPostExecute(customHttpResponse);
                    hideMessage();
                    if (customHttpResponse == null) {
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }
                    if (customHttpResponse.code == 200) {
                        showMessage("Successfully removed");
                        sendEmail("Utility expense removed", utilityExp);
                    } else if (customHttpResponse.code == 500) {
                        showMessage("Cannot remove entry");
                    } else {
                        showMessage(customHttpResponse.entity);
                    }
                }
            }.deleteUtilityExpense(utilityExp.getInt("id"));
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
        getUtilitiesExpense(offset);
    }

    public void next(View view) {
        if (currPage < totalPages) {
            offset += DashboardActivity.MAX_RESULTS;
            currPage = offset / DashboardActivity.MAX_RESULTS + 1;
            getUtilitiesExpense(offset);
        }
    }

    public void previous(View view) {
        if (currPage > 1) {
            offset -= DashboardActivity.MAX_RESULTS;
            currPage = offset / DashboardActivity.MAX_RESULTS + 1;
            getUtilitiesExpense(offset);
        }
    }

    public void sync(View view) {
        getUtilitiesExpense(offset);
    }

    public void addItem(View view) {
        Intent intent = new Intent(this, UtilityExpenseEditActivity.class);
        intent.putExtra("actionType", "add");
        startActivity(intent);
    }

    public void editItem(View view) {
        Intent intent = new Intent(this, UtilityExpenseEditActivity.class);
        intent.putExtra("actionType", "edit");
        intent.putExtra("selectedUtilityExp", selectedUtilityExp.toString());
        startActivity(intent);
    }

    public void removeItem(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage("Delete entry?")
                .setIcon(R.drawable.emoticon_grumpy)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        removeUtilityExp(selectedUtilityExp);
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    public void chartify(View view) {
        Intent intent = new Intent(this, UtilityExpChartActivity.class);
        startActivity(intent);
    }

    private void setPages(int currPage, int totalPages) {
        pagesTextView.setText("Page " + currPage + " of " + totalPages);
    }

    public void setDashboardDrawerLayout() {
        navBarView = (DrawerLayout) findViewById(R.id.drawerLayoutId);
        navBarView.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void toggleDrawerLayoutView(View view) {
        if (toggleNav % 2 == 0) {
            navBarView.openDrawer(Gravity.LEFT);
        } else {
            navBarView.closeDrawer(Gravity.LEFT);
        }
    }

    private void sendEmail(String subject, JSONObject utilityExp) {
        try {
            SimpleDateFormat sd = new SimpleDateFormat(App.DATE_FORMAT);
            String date = sd.format(utilityExp.getLong("date"));
            String body = "Utility expense:\n"
                    + "\nValue: " + utilityExp.getString("value")
                    + "\nUtility: " + utilityExp.getJSONObject("utility").get("name").toString()
                    + "\nDate: " + date
                    + "\nDescription: " + utilityExp.getString("description")
                    + "\nAction by user: " + ((App) getApplication()).getSessionUsername()
                    + "\nFor more details, access your account: http://house-angrynerds.rhcloud.com";

            JSONObject email = EmailUtil.buildEmailJson(getSelectedAddressTenantsEmails(), subject, body);

            new AppAsyncHttp(this) {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    showMessage("Notifying tenants via email...");
                }

                @Override
                protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                    super.onPostExecute(customHttpResponse);
                    if (customHttpResponse == null) {
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }
                    if (customHttpResponse.code == HttpStatus.SC_OK) {
                        showMessage("eMail sent");
                    } else {
                        if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                            showMessage(customHttpResponse.entity);
                        } else {
                            showMessage(App.GENERAL_ERROR_MSG);
                        }
                    }
                }
            }.sendMail(email.toString());
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    public JSONArray getSelectedAddressTenantsEmails() throws JSONException {
        JSONArray emails = new JSONArray();
        JSONArray tenants = selectedAddress.getJSONArray("assignedPersons");
        for (int i = 0; i < tenants.length(); i++) {
            emails.put(tenants.getJSONObject(i).getString("email"));
        }
        return emails;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }
}

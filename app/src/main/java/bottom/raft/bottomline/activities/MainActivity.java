package bottom.raft.bottomline.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.InternalStorageUtil;
import bottom.raft.bottomline.utils.WebAppException;

public class MainActivity extends AppCompatActivity {

    public static Logger log = Logger.getLogger(MainActivity.class.getName());

    private View progressBar;
    private TextView msg;
    private View loginLayout;
    private View welcomeLayout;
    private TextView sessionUsernameText;
    private EditText user;
    private EditText pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.loadingLogin);
        msg = (TextView) findViewById(R.id.msgText);
        loginLayout = findViewById(R.id.loginLayoutId);
        welcomeLayout = findViewById(R.id.welcomeLayoutId);
        sessionUsernameText = (TextView) findViewById(R.id.username);

        user = (EditText) findViewById(R.id.txtUser);
        pwd = (EditText) findViewById(R.id.txtPwd);

        showLoginLayout();
        progressBar.setVisibility(View.GONE);
        loadCredentials();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (((App) getApplication()).getAuthorization() == null) {
            showLoginLayout();
            String message = getIntent().getStringExtra("unauthorized_message");
            if (message != null) {
                msg.setText(message);
            }
        }
    }

    public void login(View view) {
        try {
            login(user.getText().toString(), pwd.getText().toString());
        } catch (JSONException | WebAppException | UnsupportedEncodingException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            msg.setText(App.GENERAL_ERROR_MSG);
        }
    }

    public void accessAddresses(View view) {
        Intent intent = new Intent(this, AddressActivity.class);
        startActivity(intent);
    }

    public void createAccount(View view) {
        Intent intent = new Intent(this, AccountActivity.class);
        startActivity(intent);
    }

    private void login(String user, String pwd) throws JSONException, WebAppException, UnsupportedEncodingException {
        JSONObject loginJson = new JSONObject();
        loginJson.put("username", user);
        loginJson.put("password", pwd);
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                msg.setText(null);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                progressBar.setVisibility(View.GONE);
                if (customHttpResponse == null) {
                    msg.setText(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    persistCredentials();
                    App app = (App) getApplication();
                    try {
                        JSONObject responseJson = new JSONObject(customHttpResponse.entity);
                        app.setSessionUser(customHttpResponse.entity);
                        app.setAuthorization(responseJson.getString("uuid"));
                        app.setSessionUserId(responseJson.getInt("userId"));
                        sessionUsernameText.setText(responseJson.getString("username"));
                        hideLoginLayout();
                    } catch (JSONException e) {
                        log.log(Level.SEVERE, e.getMessage(), e);
                        msg.setText(App.GENERAL_ERROR_MSG);
                    }
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        log.log(Level.WARNING, customHttpResponse.entity);
                        msg.setText(customHttpResponse.entity);
                    } else {
                        log.log(Level.SEVERE, customHttpResponse.entity);
                        msg.setText(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.login(loginJson.toString());
    }

    private void loadCredentials() {
        Properties props = InternalStorageUtil.readProperties(this);
        user.setText(props.getProperty("user"));
        pwd.setText(props.getProperty("pwd"));
    }

    private void persistCredentials() {
        Properties props = new Properties();
        props.put("user", user.getText().toString());
        props.put("pwd", pwd.getText().toString());
        InternalStorageUtil.persistProperties(this, props);
    }

    private void showLoginLayout() {
        loginLayout.setVisibility(View.VISIBLE);
        welcomeLayout.setVisibility(View.GONE);
    }

    private void hideLoginLayout() {
        loginLayout.setVisibility(View.GONE);
        welcomeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


        // Checks whether a hardware keyboard is available
        if (newConfig.keyboardHidden == Configuration.KEYBOARDHIDDEN_NO) {
            log.info("TEEEEEST");
            Toast.makeText(this, "keyboard visible", Toast.LENGTH_SHORT).show();
        } else if (newConfig.keyboardHidden == Configuration.KEYBOARDHIDDEN_YES) {
            log.info("TEEEEEST111");
            Toast.makeText(this, "keyboard hidden", Toast.LENGTH_SHORT).show();
        }
    }
}

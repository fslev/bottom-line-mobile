package bottom.raft.bottomline.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AddressListAdapter;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;

public class AddressActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(AddressActivity.class.getName());

    private ListView addressesList;
    private View addressDashboardBtn;

    public JSONObject selectedAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        addressesList = (ListView) findViewById(R.id.addressesList);
        addressDashboardBtn = findViewById(R.id.addressDashboardBtnId);
        editBtn = findViewById(R.id.feditBtnId);
        rmBtn = findViewById(R.id.frmBtnId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        selectedAddress = null;
        hideButtons();
        addressDashboardBtn.setVisibility(View.GONE);
        getAddresses();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getAddresses() {
        new AppAsyncHttp(this) {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Retrieving addresses...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse response) {
                super.onPostExecute(response);
                if (response == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (response.code == HttpStatus.SC_OK) {
                    JSONArray jsonArray = null;
                    try {
                        jsonArray = new JSONArray(response.entity);
                    } catch (Exception e) {
                        log.log(Level.SEVERE, e.getMessage(), e);
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }

                    AddressListAdapter addrListAdapter = new AddressListAdapter(getApplicationContext(), jsonArray);
                    addressesList.setAdapter(addrListAdapter);
                    addressesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                            selectedAddress = ((JSONObject) addressesList.getItemAtPosition(position));
                            log.info("Selected address: " + selectedAddress.toString());
                            showButtons();
                            addressDashboardBtn.setVisibility(View.VISIBLE);
                        }
                    });
                    if (jsonArray.length() == 0) {
                        showMessage("Please create a new address");
                    } else {
                        showMessage("Choose address to configure");
                    }
                } else if (response.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                    showMessage(response.entity);
                } else {
                    showMessage(App.GENERAL_ERROR_MSG);
                }
            }
        }.getAddresses();
    }

    public void removeAddress(int id) {

        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Removing...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == 200) {
                    showMessage("Address successfully removed");
                } else if (customHttpResponse.code != 500) {
                    showMessage(customHttpResponse.entity);
                } else {
                    showMessage("Cannot remove entry");
                }
                selectedAddress = null;
                hideButtons();
                addressDashboardBtn.setVisibility(View.GONE);
            }
        }.deleteAddress(id);
        getAddresses();
    }


    public void next(View view) {
        if (selectedAddress == null) {
            showMessage("Please, select an address!");
            return;
        }
        Intent intent = new Intent(this, DashboardActivity.class);
        ((App) getApplication()).setSelectedAddress(selectedAddress.toString());
        startActivity(intent);
        finish();
    }

    public void addItem(View view) {
        Intent intent = new Intent(this, AddressEditActivity.class);
        intent.putExtra("actionType", "add");
        startActivity(intent);
    }

    public void editItem(View view) {
        Intent intent = new Intent(this, AddressEditActivity.class);
        intent.putExtra("actionType", "edit");
        ((App) getApplication()).setSelectedAddress(selectedAddress.toString());
        startActivity(intent);
    }

    public void removeItem(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage("Delete entry?")
                .setIcon(R.drawable.emoticon_grumpy)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            removeAddress(selectedAddress.getInt("id"));
                        } catch (JSONException e) {
                            log.log(Level.SEVERE, e.getMessage(), e);
                            showMessage(App.GENERAL_ERROR_MSG);
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}

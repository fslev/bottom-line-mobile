package bottom.raft.bottomline.activities.charts;

import android.app.DatePickerDialog;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.activities.App;
import bottom.raft.bottomline.utils.AbstractChart;

/**
 * Created by raft on 20.02.2016.
 */
public abstract class AbstractChartActivity extends AppCompatActivity {

    public static final String DEFAULT_MESSAGE = "Rotate screen for better chart visualization";

    protected long startDate;
    protected long endDate;
    protected AbstractChart chart;
    protected Map<String, List<Entry>> selectedDatasets = new HashMap<>();

    protected LineChart lineChart;

    protected View messageLayout;
    protected TextView messageText;
    protected EditText startDateEditText;
    protected EditText endDateEditText;
    protected LinearLayout checkboxesLayout;
    protected LinearLayout chartMenuLayout;

    protected void initViews() {
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);

        startDateEditText = (EditText) findViewById(R.id.startDateId);
        endDateEditText = (EditText) findViewById(R.id.endDateId);
        checkboxesLayout = (LinearLayout) findViewById(R.id.checkboxesLayoutId);
        chartMenuLayout = (LinearLayout) findViewById(R.id.chartMenuId);
        lineChart = (LineChart) findViewById(R.id.chartId);
        lineChart.setDescription("Evolution");
        lineChart.setDescriptionColor(Color.LTGRAY);

        startDate = Calendar.getInstance().getTimeInMillis() - 1000L * 60 * 60 * 24 * 365;
        endDate = Calendar.getInstance().getTimeInMillis();
        updateDateEditText(startDateEditText, startDate);
        updateDateEditText(endDateEditText, endDate);

        addDatePickerToStartDateText(startDateEditText);
        addDatePickerToEndDateText(endDateEditText);
    }

    protected void addDatePickerToStartDateText(final EditText dateEditText) {
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                startDate = myCalendar.getTimeInMillis();
                updateDateEditText(dateEditText, startDate);
            }
        };

        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AbstractChartActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    protected void addDatePickerToEndDateText(final EditText dateEditText) {
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                endDate = myCalendar.getTimeInMillis();
                updateDateEditText(dateEditText, endDate);
            }
        };

        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AbstractChartActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    protected void updateDateEditText(EditText dateEditText, long timeInMillis) {
        SimpleDateFormat sd = new SimpleDateFormat(App.DATE_FORMAT);
        dateEditText.setText(sd.format(timeInMillis));
    }

    protected void refreshCheckboxes() {
        checkboxesLayout.removeAllViews();
        selectedDatasets.clear();
        for (final Map.Entry<String, List<Entry>> entry : chart.getDataSets().entrySet()) {
            CheckBox checkbox = new CheckBox(this);
            checkbox.setText(entry.getKey());
            checkbox.setTextSize(12);
            checkbox.setTextColor(0xff089ab1); //blue
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        selectedDatasets.put(entry.getKey(), entry.getValue());
                    } else {
                        selectedDatasets.remove(entry.getKey());
                    }
                }
            });
            checkboxesLayout.addView(checkbox);
            if (entry.getKey().equals("Total")) {
                checkbox.setChecked(true);
            }
        }
    }

    protected void redrawChart(List<String> xLabels, Map<String, List<Entry>> datasets) {
        LineData data = new LineData(xLabels);
        int i = 0;
        for (Map.Entry<String, List<Entry>> entry : datasets.entrySet()) {
            LineDataSet dataset = new LineDataSet(entry.getValue(), entry.getKey());
            dataset.setDrawFilled(true);
            dataset.setDrawCubic(true);
            int color = AbstractChart.colors.get(i++);
            dataset.setColor(color);
            dataset.setFillColor(color);
            data.addDataSet(dataset);
        }
        lineChart.setData(data);
        lineChart.invalidate();
    }

    protected abstract void drawChart();


    public void redrawChart(View view) {
        for (Map.Entry<String, List<Entry>> entry : selectedDatasets.entrySet()) {
            redrawChart(chart.getxLabels(), selectedDatasets);
        }
    }

    public void applyTimeInterval(View view) {
        drawChart();
    }

    protected void showMessage(String msg) {
        messageText.setText(msg);
        messageLayout.setVisibility(View.VISIBLE);
    }

    protected void hideMessage() {
        messageLayout.setVisibility(View.GONE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            chartMenuLayout.setVisibility(View.GONE);
            System.out.println("LANDSCAPE");
        } else {
            chartMenuLayout.setVisibility(View.VISIBLE);
        }
    }
}

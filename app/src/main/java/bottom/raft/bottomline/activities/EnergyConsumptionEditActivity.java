package bottom.raft.bottomline.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.ViewUtil;

public class EnergyConsumptionEditActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(EnergyConsumptionEditActivity.class.getName());

    private DrawerLayout navBarView;
    private TextView actionTitleText;
    private View applyButton;
    private TextView dateText;
    private TextView indexText;
    private TextView consText;
    private TextView descText;


    private JSONObject selectedEnergyCons;
    private JSONObject selectedAddress;

    private int toggleNav;
    private String actionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_energy_cons_edit);
        setDashboardDrawerLayout();

        actionType = getIntent().getStringExtra("actionType");
        actionTitleText = (TextView) findViewById(R.id.actionTitleId);
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        applyButton = findViewById(R.id.fapplyBtnId);

        dateText = (TextView) findViewById(R.id.energyConsDateId);
        indexText = (TextView) findViewById(R.id.energyConsIndexId);
        consText = (TextView) findViewById(R.id.energyConsId);
        descText = (TextView) findViewById(R.id.energyConsDescId);

        try {
            selectedAddress = new JSONObject(((App) getApplication()).getSelectedAddress());
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
            return;
        }

        hideMessage();
        addDatePickerToDateText();
    }

    @Override
    protected void onResume() {
        super.onResume();
        switch (actionType) {
            case "add":
                actionTitleText.setText("Add a new index");
                updateDateText(Calendar.getInstance());
                break;
            case "edit":
                actionTitleText.setText("Edit index");
                try {
                    selectedEnergyCons = new JSONObject(getIntent().getStringExtra("selectedEnergyCons"));
                } catch (JSONException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }

                Calendar calendar = Calendar.getInstance();
                try {
                    calendar.setTimeInMillis(selectedEnergyCons.getLong("date"));
                    updateDateText(calendar);
                    ViewUtil.setTextViewValue(indexText, selectedEnergyCons.getString("index"));
                    ViewUtil.setTextViewValue(consText, selectedEnergyCons.getString("consumption"));
                    ViewUtil.setTextViewValue(descText, selectedEnergyCons.getString("description"));
                } catch (JSONException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                }
                break;
        }
    }

    private void addDatePickerToDateText() {
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateText(myCalendar);
            }
        };

        dateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EnergyConsumptionEditActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateDateText(Calendar calendar) {
        SimpleDateFormat sd = new SimpleDateFormat(App.DATE_FORMAT);
        dateText.setText(sd.format(calendar.getTime()));
    }

    public void setDashboardDrawerLayout() {

        navBarView = (DrawerLayout) findViewById(R.id.drawerLayoutId);
        navBarView.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void apply(View view) {
        switch (actionType) {
            case "add":
                try {
                    addEnergyCons();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                }
                break;
            case "edit":
                try {
                    updateEnergyCons();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                }
                break;
        }
    }

    private void updateEnergyCons() throws JSONException, ParseException, UnsupportedEncodingException {
        final JSONObject data = buildJsonData();
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Updating...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.updateEnergyCons(data.toString(), selectedEnergyCons.getInt("id"));
    }

    private void addEnergyCons() throws JSONException, ParseException, UnsupportedEncodingException {
        final JSONObject data = buildJsonData();
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.addEnergyCons(data.toString(), selectedAddress.getInt("id"));
    }

    private JSONObject buildJsonData() throws JSONException, ParseException {
        JSONObject json = new JSONObject();
        json.put("index", indexText.getText());
        json.put("consumption", (!consText.getText().toString().isEmpty()
                && !consText.getText().toString().matches("\\d+")) ? null : consText.getText());
        json.put("description", descText.getText());
        SimpleDateFormat sd = new SimpleDateFormat(App.DATE_FORMAT);
        json.put("date", sd.parse(dateText.getText().toString()).getTime());
        return json;
    }

    public void toggleDrawerLayoutView(View view) {
        if (toggleNav % 2 == 0) {
            navBarView.openDrawer(Gravity.LEFT);
        } else {
            navBarView.closeDrawer(Gravity.LEFT);
        }
    }

    public void showDialog(View view) {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_energy_cons_edit);
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, EnergyConsumptionActivity.class));
        finish();
    }
}

package bottom.raft.bottomline.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;

public class RoomEditActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(RoomEditActivity.class.getName());

    private TextView roomText;
    private View titleLayout;
    private TextView titleText;

    private String actionType;
    private JSONObject selectedRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_edit);

        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        titleText = (TextView) findViewById(R.id.configTitleId);
        roomText = (TextView) findViewById(R.id.roomId);

        hideMessage();

        actionType = getIntent().getStringExtra("actionType");
    }


    @Override
    protected void onStart() {
        super.onStart();
        switch (actionType) {
            case "add":
                titleText.setText("Add room");
                break;
            case "edit":
                titleText.setText("Edit room");
                try {
                    selectedRoom = new JSONObject(getIntent().getStringExtra("selectedRoom"));
                } catch (JSONException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                populateFields(selectedRoom);
                break;
        }
    }

    private void populateFields(JSONObject roomJson) {
        try {
            roomText.setText(roomJson.getString("name"));
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    public void apply(View view) {
        if (roomText.getText().length() == 0) {
            showMessage("No empty fields are allowed");
            return;
        }
        JSONObject roomJson = null;
        try {
            roomJson = buildJsonData();
            switch (actionType) {
                case "add":
                    addRoom(roomJson.toString(), ((App) getApplication()).getSelectedAddressId());
                    break;
                case "edit":
                    updateRoom(selectedRoom.getInt("id"), roomJson.toString());
                    break;
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }

    }

    private void addRoom(String room, int addressId) throws UnsupportedEncodingException {
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                    refreshSelectedAddress();
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.addRoom(room, addressId);
    }

    private void updateRoom(int roomId, String room) throws UnsupportedEncodingException {
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                    refreshSelectedAddress();
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.updateRoom(roomId, room);
    }

    private JSONObject buildJsonData() throws JSONException, ParseException {
        JSONObject json = new JSONObject();
        json.put("name", roomText.getText());
        return json;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

package bottom.raft.bottomline.activities;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONException;

import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;

/**
 * Created by raft on 13.02.2016.
 */
public class CustomEditActivity extends AppCompatActivity {

    public static final Logger log = Logger.getLogger(CustomEditActivity.class.getName());

    protected View messageLayout;
    protected TextView messageText;
    protected View editBtn;
    protected View rmBtn;

    protected void showMessage(String msg) {
        messageText.setText(msg);
        messageLayout.setVisibility(View.VISIBLE);
    }

    protected void hideMessage() {
        messageLayout.setVisibility(View.GONE);
    }


    protected void refreshSelectedAddress() {
        try {
            new AppAsyncHttp(this) {

                @Override
                protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                    super.onPostExecute(customHttpResponse);
                    if (customHttpResponse == null) {
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }
                    if (customHttpResponse.code == HttpStatus.SC_OK) {
                        ((App) getApplication()).setSelectedAddress(customHttpResponse.entity);
                        log.info("Selected address refreshed: " + ((App) getApplication()).getSelectedAddress());
                    } else {
                        if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                            showMessage(customHttpResponse.entity);
                        } else {
                            showMessage(App.GENERAL_ERROR_MSG);
                        }
                    }
                }
            }.getAddress(((App) getApplication()).getSelectedAddressId());
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    protected void hideButtons() {
        editBtn.setVisibility(View.GONE);
        rmBtn.setVisibility(View.GONE);
    }

    protected void showButtons() {
        editBtn.setVisibility(View.VISIBLE);
        rmBtn.setVisibility(View.VISIBLE);
    }
}

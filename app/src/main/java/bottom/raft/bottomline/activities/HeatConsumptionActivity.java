package bottom.raft.bottomline.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.activities.charts.HeatConsChartActivity;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.HeatListAdapter;

public class HeatConsumptionActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(HeatConsumptionActivity.class.getName());

    public static final String HEAT_CONS_MSG = "heatConsMsg";

    private DrawerLayout navBarView;
    private ListView heatList;
    private TextView pagesTextView;
    private View paginationLayoutView;

    private JSONObject selectedAddress;
    private JSONObject selectedHeatCons;
    private int toggleNav;
    private int totalPages = 1;
    private int currPage = 1;
    private int offset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heat_cons);
        setDashboardDrawerLayout();

        try {
            selectedAddress = new JSONObject(((App) getApplication()).getSelectedAddress());
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            return;
        }

        heatList = (ListView) findViewById(R.id.heatList);
        paginationLayoutView = findViewById(R.id.paginationLayoutId);
        pagesTextView = (TextView) findViewById(R.id.pagesTextId);
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        editBtn = findViewById(R.id.feditBtnId);
        rmBtn = findViewById(R.id.frmBtnId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideMessage();
        hideButtons();
        setPages(currPage, totalPages);
        getHeatConsumption(offset);
    }

    public void getHeatConsumption(final int offset) {
        try {
            new AppAsyncHttp(this) {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    showMessage(App.GENERAL_LOADING_MSG);
                }

                @Override
                protected void onPostExecute(CustomHttpResponse response) {
                    super.onPostExecute(response);
                    hideButtons();
                    if (response == null) {
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }

                    if (response.code == HttpStatus.SC_OK) {
                        JSONArray jsonArray = null;
                        try {
                            jsonArray = new JSONArray(response.entity);
                        } catch (JSONException e) {
                            showMessage(App.GENERAL_ERROR_MSG);
                            return;
                        }

                        int count = Integer.parseInt(response.getHeaderValue("count"));
                        totalPages = (count == 0) ? 1 : (int) Math.ceil((double) count / DashboardActivity.MAX_RESULTS);
                        setPages(currPage, totalPages);

                        HeatListAdapter heatListAdapter = new HeatListAdapter(getApplicationContext(), jsonArray);
                        heatList.setAdapter(heatListAdapter);
                        heatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                            @Override
                                                            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                                                                selectedHeatCons = ((JSONObject) heatList.getItemAtPosition(position));
                                                                showButtons();
                                                            }
                                                        }
                        );
                        if (jsonArray.length() == 0 && offset == 0) {
                            showMessage("No entries. Please add one");
                        } else {
                            hideMessage();
                        }
                    } else {
                        showMessage(response.entity);
                    }

                }
            }.getHeatConsList(selectedAddress.getInt("id"), offset, DashboardActivity.MAX_RESULTS);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }


    public void removeHeatConsumption(int id) {
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Removing...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                hideMessage();
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == 500) {
                    showMessage("Cannot remove entry");
                } else {
                    showMessage(customHttpResponse.entity);
                }
            }
        }.deleteHeatCons(id);
        getHeatConsumption(offset);
    }

    public void next(View view) {
        if (currPage < totalPages) {
            offset += DashboardActivity.MAX_RESULTS;
            currPage = offset / DashboardActivity.MAX_RESULTS + 1;
            getHeatConsumption(offset);
        }
    }

    public void previous(View view) {
        if (currPage > 1) {
            offset -= DashboardActivity.MAX_RESULTS;
            currPage = offset / DashboardActivity.MAX_RESULTS + 1;
            getHeatConsumption(offset);
        }
    }

    public void sync(View view) {
        getHeatConsumption(offset);
    }

    public void addItem(View view) {
        Intent intent = new Intent(this, HeatConsumptionEditActivity.class);
        intent.putExtra("actionType", "add");
        startActivity(intent);
    }

    public void editItem(View view) {
        Intent intent = new Intent(this, HeatConsumptionEditActivity.class);
        intent.putExtra("actionType", "edit");
        intent.putExtra("selectedHeatCons", selectedHeatCons.toString());
        startActivity(intent);
    }

    public void removeItem(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage("Delete entry?")
                .setIcon(R.drawable.emoticon_grumpy)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            removeHeatConsumption(selectedHeatCons.getInt("id"));
                        } catch (JSONException e) {
                            log.log(Level.SEVERE, e.getMessage(), e);
                            showMessage(App.GENERAL_ERROR_MSG);
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    public void chartify(View view) {
        Intent intent = new Intent(this, HeatConsChartActivity.class);
        startActivity(intent);
    }

    private void setPages(int currPage, int totalPages) {
        pagesTextView.setText("Page " + currPage + " of " + totalPages);
    }

    public void setDashboardDrawerLayout() {
        navBarView = (DrawerLayout) findViewById(R.id.drawerLayoutId);
        navBarView.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void toggleDrawerLayoutView(View view) {
        if (toggleNav % 2 == 0) {
            navBarView.openDrawer(Gravity.LEFT);
        } else {
            navBarView.closeDrawer(Gravity.LEFT);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, DashboardActivity.class));
        finish();
    }
}

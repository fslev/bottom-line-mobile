package bottom.raft.bottomline.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.RoomListAdapter;

/**
 * Created by raft on 12.02.2016.
 */
public class RoomActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(RoomActivity.class.getName());

    private TextView countryText;
    private TextView cityText;
    private TextView streetText;

    ListView roomListView;

    private JSONObject selectedRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        countryText = (TextView) findViewById(R.id.countryId);
        cityText = (TextView) findViewById(R.id.cityId);
        streetText = (TextView) findViewById(R.id.streetId);
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        roomListView = (ListView) findViewById(R.id.roomListViewId);

        editBtn = findViewById(R.id.feditBtnId);
        rmBtn = findViewById(R.id.frmBtnId);
    }

    @Override
    protected void onStart() {
        super.onStart();
        selectedRoom = null;
        hideButtons();
        hideMessage();
        try {
            ((App) getApplication()).populateSelectedAddress(countryText, cityText, streetText);
            refreshSelectedAddressAndPopulateRoomList();
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    private void refreshSelectedAddressAndPopulateRoomList() throws JSONException {
        selectedRoom = null;
        new AppAsyncHttp(this) {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Refresh address and get rooms...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                hideMessage();
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    ((App) getApplication()).setSelectedAddress(customHttpResponse.entity);
                    log.info("Selected address refreshed: " + ((App) getApplication()).getSelectedAddress());

                    JSONArray roomsJsonArray = null;
                    try {
                        roomsJsonArray = ((App) getApplication()).getSelectedAddressRoomList();
                        log.info("Got rooms from selected address: " + roomsJsonArray.toString());
                    } catch (JSONException e) {
                        log.log(Level.SEVERE, e.getMessage(), e);
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }
                    if (roomsJsonArray == null) {
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }
                    RoomListAdapter adapter = new RoomListAdapter(getApplicationContext(), roomsJsonArray);
                    roomListView.setAdapter(adapter);
                    roomListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                            selectedRoom = ((JSONObject) roomListView.getItemAtPosition(position));
                            log.info("Selected room: " + selectedRoom.toString());
                            showButtons();
                        }
                    });
                    if (roomsJsonArray.length() == 0) {
                        showMessage("No rooms configured for this address");
                    } else {
                        showMessage("Choose a room to configure");
                    }
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.getAddress(((App) getApplication()).getSelectedAddressId());
    }

    public void addItem(View view) {
        Intent intent = new Intent(this, RoomEditActivity.class);
        intent.putExtra("actionType", "add");
        startActivity(intent);
    }

    public void editItem(View view) {
        Intent intent = new Intent(this, RoomEditActivity.class);
        intent.putExtra("selectedRoom", selectedRoom.toString());
        intent.putExtra("actionType", "edit");
        startActivity(intent);
    }

    public void removeItem(View view) {
        new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage("Delete entry?")
                .setIcon(R.drawable.emoticon_grumpy)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            removeRoom(selectedRoom.getInt("id"));
                            refreshSelectedAddressAndPopulateRoomList();
                        } catch (JSONException e) {
                            log.log(Level.SEVERE, e.getMessage(), e);
                            showMessage(App.GENERAL_ERROR_MSG);
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    public void removeRoom(int id) {
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Removing...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == 200) {
                    showMessage("Room successfully removed");
                } else if (customHttpResponse.code == 500) {
                    showMessage("Cannot remove entry");
                } else {
                    showMessage(customHttpResponse.entity);
                }
                selectedRoom = null;
                hideButtons();
            }
        }.deleteRoom(id);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

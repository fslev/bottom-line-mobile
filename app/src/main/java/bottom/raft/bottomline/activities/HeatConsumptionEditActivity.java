package bottom.raft.bottomline.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.ViewUtil;

public class HeatConsumptionEditActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(HeatConsumptionEditActivity.class.getName());

    private DrawerLayout navBarView;
    private TextView actionTitleText;
    private View applyButton;
    private TextView dateText;
    private Spinner roomSpinner;
    private TextView indexText;
    private TextView consText;
    private TextView descText;


    private JSONObject selectedHeatCons;
    private JSONObject selectedAddress;
    private Integer selectedRoomId;
    private Integer selectedHeatConsRoomId;
    private List<Integer> roomIds = new ArrayList<>();

    private int toggleNav;
    private String actionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heat_cons_edit);
        setDashboardDrawerLayout();

        actionType = getIntent().getStringExtra("actionType");
        actionTitleText = (TextView) findViewById(R.id.actionTitleId);
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        applyButton = findViewById(R.id.fapplyBtnId);

        dateText = (TextView) findViewById(R.id.heatConsDateId);
        roomSpinner = (Spinner) findViewById(R.id.heatConsRoomId);
        indexText = (TextView) findViewById(R.id.heatConsIndexId);
        consText = (TextView) findViewById(R.id.heatConsId);
        descText = (TextView) findViewById(R.id.heatConsDescId);

        try {
            selectedAddress = new JSONObject(((App) getApplication()).getSelectedAddress());
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
            return;
        }

        hideMessage();
        checkAddressHasRooms();
        addDatePickerToDateText();
        addItemsToRoomSpinner();
    }

    @Override
    protected void onResume() {
        super.onResume();
        switch (actionType) {
            case "add":
                actionTitleText.setText("Add a new index");
                updateDateText(Calendar.getInstance());
                break;
            case "edit":
                actionTitleText.setText("Edit index");
                try {
                    selectedHeatCons = new JSONObject(getIntent().getStringExtra("selectedHeatCons"));
                    selectedHeatConsRoomId = selectedHeatCons.getJSONObject("room").getInt("id");
                } catch (JSONException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }

                Calendar calendar = Calendar.getInstance();
                try {
                    calendar.setTimeInMillis(selectedHeatCons.getLong("date"));
                    updateDateText(calendar);
                    updateRoomSpinner(selectedHeatConsRoomId);
                    ViewUtil.setTextViewValue(indexText, selectedHeatCons.getString("index"));
                    ViewUtil.setTextViewValue(consText, selectedHeatCons.getString("consumption"));
                    ViewUtil.setTextViewValue(descText, selectedHeatCons.getString("description"));
                } catch (JSONException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                }
                break;
        }
    }

    private void addItemsToRoomSpinner() {
        final List<String> items = new ArrayList<>();
        roomIds.clear();
        try {
            JSONArray array = selectedAddress.getJSONArray("rooms");
            for (int i = 0; i < array.length(); i++) {
                JSONObject value = array.getJSONObject(i);
                items.add(value.getString("name"));
                //order ids
                roomIds.add(value.getInt("id"));
            }
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
        roomSpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items));
        roomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedRoomId = roomIds.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void addDatePickerToDateText() {
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateText(myCalendar);
            }
        };

        dateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(HeatConsumptionEditActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateRoomSpinner(int roomId) {
        for (int i = 0; i < roomIds.size(); i++) {
            if (roomIds.get(i) == roomId) {
                roomSpinner.setSelection(i);
                break;
            }
        }
    }

    private void updateDateText(Calendar calendar) {
        SimpleDateFormat sd = new SimpleDateFormat(App.DATE_FORMAT);
        dateText.setText(sd.format(calendar.getTime()));
    }

    private void checkAddressHasRooms() {
        try {
            if (selectedAddress.getJSONArray("rooms").length() == 0) {
                applyButton.setVisibility(View.GONE);
                showMessage("Current address has no rooms. Slide from left to view dashboard menu and go to your addresses");
            }
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    public void setDashboardDrawerLayout() {

        navBarView = (DrawerLayout) findViewById(R.id.drawerLayoutId);
        navBarView.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void apply(View view) {
        switch (actionType) {
            case "add":
                try {
                    addHeatCons();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                }
                break;
            case "edit":
                try {
                    updateHeatCons();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                }
                break;
        }
    }

    private void updateHeatCons() throws JSONException, ParseException, UnsupportedEncodingException {
        final JSONObject data = buildJsonData();
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Updating...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.updateHeatCons(data.toString(), selectedHeatCons.getInt("id"), selectedRoomId);
    }

    private void addHeatCons() throws JSONException, ParseException, UnsupportedEncodingException {
        final JSONObject data = buildJsonData();
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.addHeatCons(data.toString(), selectedAddress.getInt("id"), selectedRoomId);
    }

    private JSONObject buildJsonData() throws JSONException, ParseException {
        JSONObject json = new JSONObject();
        json.put("index", indexText.getText());
        json.put("consumption", (!consText.getText().toString().isEmpty()
                && !consText.getText().toString().matches("\\d+")) ? null : consText.getText());
        json.put("description", descText.getText());
        SimpleDateFormat sd = new SimpleDateFormat(App.DATE_FORMAT);
        json.put("date", sd.parse(dateText.getText().toString()).getTime());
        json.put("room", roomSpinner.getSelectedItem().toString());
        return json;
    }

    public void toggleDrawerLayoutView(View view) {
        if (toggleNav % 2 == 0) {
            navBarView.openDrawer(Gravity.LEFT);
        } else {
            navBarView.closeDrawer(Gravity.LEFT);
        }
    }

    public void showDialog(View view) {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_heat_cons_edit);
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, HeatConsumptionActivity.class));
        finish();
    }
}

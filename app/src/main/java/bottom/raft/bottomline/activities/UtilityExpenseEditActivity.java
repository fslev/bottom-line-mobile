package bottom.raft.bottomline.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.EmailUtil;
import bottom.raft.bottomline.utils.ViewUtil;

public class UtilityExpenseEditActivity extends CustomEditActivity {

    public static final Logger log = Logger.getLogger(UtilityExpenseEditActivity.class.getName());

    private DrawerLayout navBarView;
    private TextView actionTitleText;
    private View applyButton;
    private TextView dateText;
    private Spinner utilitySpinner;
    private TextView valueText;
    private TextView descText;


    private JSONObject selectedUtilityExp;
    private JSONObject selectedAddress;
    private JSONObject selectedUtility;
    private Integer selectedUtilityId;
    private Integer selectedUtilityExpUtilityId;
    private List<Integer> utilityIds = new ArrayList<>();
    private List<JSONObject> utilities = new ArrayList<>();

    private int toggleNav;
    private String actionType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility_exp_edit);
        setDashboardDrawerLayout();

        actionType = getIntent().getStringExtra("actionType");
        actionTitleText = (TextView) findViewById(R.id.actionTitleId);
        messageLayout = findViewById(R.id.messageId);
        messageText = (TextView) findViewById(R.id.messageTextId);
        applyButton = findViewById(R.id.fapplyBtnId);

        dateText = (TextView) findViewById(R.id.utilityExpDateId);
        utilitySpinner = (Spinner) findViewById(R.id.utilityExpUtilityId);
        valueText = (TextView) findViewById(R.id.utilityExpValueId);
        descText = (TextView) findViewById(R.id.utilityExpDescId);

        try {
            selectedAddress = new JSONObject(((App) getApplication()).getSelectedAddress());
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
            return;
        }

        hideMessage();
        checkAddressHasUtilities();
        addDatePickerToDateText();
        addItemsToUtilitySpinner();
    }

    @Override
    protected void onResume() {
        super.onResume();
        switch (actionType) {
            case "add":
                actionTitleText.setText("Add a new utility expense");
                updateDateText(Calendar.getInstance());
                break;
            case "edit":
                actionTitleText.setText("Edit utility expense");
                try {
                    selectedUtilityExp = new JSONObject(getIntent().getStringExtra("selectedUtilityExp"));
                    selectedUtilityExpUtilityId = selectedUtilityExp.getJSONObject("utility").getInt("id");
                } catch (JSONException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }

                Calendar calendar = Calendar.getInstance();
                try {
                    calendar.setTimeInMillis(selectedUtilityExp.getLong("date"));
                    updateDateText(calendar);
                    updateUtilitySpinner(selectedUtilityExpUtilityId);
                    ViewUtil.setTextViewValue(valueText, selectedUtilityExp.getString("value"));
                    ViewUtil.setTextViewValue(descText, selectedUtilityExp.getString("description"));
                } catch (JSONException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                }
                break;
        }
    }

    private void addItemsToUtilitySpinner() {
        final List<String> items = new ArrayList<>();
        utilityIds.clear();
        try {
            JSONArray array = selectedAddress.getJSONArray("utilities");
            for (int i = 0; i < array.length(); i++) {
                JSONObject value = array.getJSONObject(i);
                items.add(value.getString("name"));
                //order ids
                utilityIds.add(value.getInt("id"));
                utilities.add(value);
            }
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
        utilitySpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items));
        utilitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedUtilityId = utilityIds.get(position);
                selectedUtility = utilities.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void addDatePickerToDateText() {
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateText(myCalendar);
            }
        };

        dateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(UtilityExpenseEditActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateUtilitySpinner(int utilityId) {
        for (int i = 0; i < utilityIds.size(); i++) {
            if (utilityIds.get(i).equals(utilityId)) {
                utilitySpinner.setSelection(i);
                break;
            }
        }
    }

    private void updateDateText(Calendar calendar) {
        SimpleDateFormat sd = new SimpleDateFormat(App.DATE_FORMAT);
        dateText.setText(sd.format(calendar.getTime()));
    }

    private void checkAddressHasUtilities() {
        try {
            if (selectedAddress.getJSONArray("utilities").length() == 0) {
                applyButton.setVisibility(View.GONE);
                showMessage("Current address has no utilities configured. Slide from left to view dashboard menu and go to your addresses");
            }
        } catch (JSONException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    public void setDashboardDrawerLayout() {

        navBarView = (DrawerLayout) findViewById(R.id.drawerLayoutId);
        navBarView.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                toggleNav++;
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void apply(View view) {
        switch (actionType) {
            case "add":
                try {
                    addUtilityExp();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                }
                break;
            case "edit":
                try {
                    updateUtilityExp();
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    showMessage(App.GENERAL_ERROR_MSG);
                }
                break;
        }
    }

    private void updateUtilityExp() throws JSONException, ParseException, UnsupportedEncodingException {
        final JSONObject data = buildJsonData();
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage("Updating...");
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                    sendEmail("Utility expense updated", data);
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.updateUtilityExpense(data.toString(), selectedUtilityExp.getInt("id"));
    }

    private void addUtilityExp() throws JSONException, ParseException, UnsupportedEncodingException {
        final JSONObject data = buildJsonData();
        new AppAsyncHttp(this) {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showMessage(App.GENERAL_LOADING_MSG);
            }

            @Override
            protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                super.onPostExecute(customHttpResponse);
                if (customHttpResponse == null) {
                    showMessage(App.GENERAL_ERROR_MSG);
                    return;
                }
                if (customHttpResponse.code == HttpStatus.SC_OK) {
                    showMessage("SUCCESS");
                    sendEmail("New utility expense added", data);
                } else {
                    if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                        showMessage(customHttpResponse.entity);
                    } else {
                        showMessage(App.GENERAL_ERROR_MSG);
                    }
                }
            }
        }.addUtilityExpense(data.toString(), selectedUtilityId);
    }

    private JSONObject buildJsonData() throws JSONException, ParseException {
        JSONObject json = new JSONObject();
        json.put("value", valueText.getText());
        json.put("description", descText.getText());
        SimpleDateFormat sd = new SimpleDateFormat(App.DATE_FORMAT);
        json.put("date", sd.parse(dateText.getText().toString()).getTime());
        json.put("utility", selectedUtility);
        return json;
    }

    public void toggleDrawerLayoutView(View view) {
        if (toggleNav % 2 == 0) {
            navBarView.openDrawer(Gravity.LEFT);
        } else {
            navBarView.closeDrawer(Gravity.LEFT);
        }
    }

    public void showDialog(View view) {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_utility_exp_edit);
        dialog.show();
    }

    private void sendEmail(String subject, JSONObject utilityExp) {
        try {
            SimpleDateFormat sd = new SimpleDateFormat(App.DATE_FORMAT);
            String date = sd.format(utilityExp.getLong("date"));
            String body = "Utility expense:\n"
                    + "\nValue: " + utilityExp.getString("value")
                    + "\nUtility: " + utilityExp.getJSONObject("utility").get("name").toString()
                    + "\nDate: " + date
                    + "\nDescription: " + utilityExp.getString("description")
                    + "\nAction by user: " + ((App) getApplication()).getSessionUsername()
                    + "\nFor more details, access your account: http://house-angrynerds.rhcloud.com";

            JSONObject email = EmailUtil.buildEmailJson(getSelectedAddressTenantsEmails(), subject, body);

            new AppAsyncHttp(this) {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    showMessage("Notifying tenants via email...");
                }

                @Override
                protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                    super.onPostExecute(customHttpResponse);
                    if (customHttpResponse == null) {
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }
                    if (customHttpResponse.code == HttpStatus.SC_OK) {
                        showMessage("eMail sent");
                    } else {
                        if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                            showMessage(customHttpResponse.entity);
                        } else {
                            showMessage(App.GENERAL_ERROR_MSG);
                        }
                    }
                }
            }.sendMail(email.toString());
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }

    public JSONArray getSelectedAddressTenantsEmails() throws JSONException {
        JSONArray emails = new JSONArray();
        JSONArray tenants = selectedAddress.getJSONArray("assignedPersons");
        for (int i = 0; i < tenants.length(); i++) {
            emails.put(tenants.getJSONObject(i).getString("email"));
        }
        return emails;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, UtilityExpenseActivity.class));
        finish();
    }
}

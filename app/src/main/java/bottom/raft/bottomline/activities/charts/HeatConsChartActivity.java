package bottom.raft.bottomline.activities.charts;

import android.os.Bundle;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.logging.Level;
import java.util.logging.Logger;

import bottom.raft.bottomline.R;
import bottom.raft.bottomline.activities.App;
import bottom.raft.bottomline.utils.AppAsyncHttp;
import bottom.raft.bottomline.utils.CustomHttpResponse;
import bottom.raft.bottomline.utils.HeatChart;

/**
 * Created by raft on 18.02.2016.
 */
public class HeatConsChartActivity extends AbstractChartActivity {

    public static Logger log = Logger.getLogger(HeatConsChartActivity.class.getName());


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heatcons_mpchart);

        initViews();
        showMessage(DEFAULT_MESSAGE);
        drawChart();
    }

    @Override
    protected void drawChart() {

        try {
            new AppAsyncHttp(this) {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    showMessage("Retrieving heat consumption entries...");
                }

                @Override
                protected void onPostExecute(CustomHttpResponse customHttpResponse) {
                    super.onPostExecute(customHttpResponse);
                    if (customHttpResponse == null) {
                        showMessage(App.GENERAL_ERROR_MSG);
                        return;
                    }
                    if (customHttpResponse.code == HttpStatus.SC_OK) {
                        showMessage(DEFAULT_MESSAGE);
                        try {
                            JSONArray jsonData = new JSONArray(customHttpResponse.entity);
                            if (jsonData.length() == 0) {
                                showMessage("No heat consumption entries found for current address");
                                return;
                            }
                            chart = new HeatChart(jsonData);
                            refreshCheckboxes();
                            redrawChart(chart.getxLabels(), selectedDatasets);
                        } catch (JSONException e) {
                            log.log(Level.SEVERE, e.getMessage(), e);
                            showMessage(App.GENERAL_ERROR_MSG);
                        }
                    } else {
                        if (customHttpResponse.code != HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                            showMessage(customHttpResponse.entity);
                        } else {
                            showMessage(App.GENERAL_ERROR_MSG);
                        }
                    }
                }
            }.getHeatConsList(((App) getApplication()).getSelectedAddressId(), 0, 5000, "date>=" + startDate + " and date<=" + endDate);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            showMessage(App.GENERAL_ERROR_MSG);
        }
    }
}
